extern "C" {
    #include "MenuChooser.h"
    #include "menu_book_reader.h"
    #include "SDL_helper.h"
    #include "common.h"
    #include "textures.h"
    #include "config.h"
}

#include <switch.h>
#include <iostream>
#include <filesystem>
#include <bits/stdc++.h>

#include <libconfig.h>

#include <SDL2/SDL_image.h>

using namespace std;
namespace fs = filesystem;

/*This is for the colors array, don't have a great way of calculating it so i use this definition*/
#define COLORS_SIZE 24

/*Default Scroll Speed*/
#define SCROLL 3

/*Default Zoom Amount*/
#define ZOOM 0.3

/*configFile For Options*/
config_t *optionConfig = NULL;
char* optionFile = "/switch/eBookReader/options.cfg";


template <typename T> bool contains(list<T> & listOfElements, const T & element) {
	auto it = find(listOfElements.begin(), listOfElements.end(), element);
	return it != listOfElements.end();
}

extern TTF_Font *ROBOTO_35, *ROBOTO_25, *ROBOTO_15;

static int load_config(unsigned int* chosenFolderColor, unsigned int* chosenBookColor, int* scroll_speed, int* zoom_amount, bool* configDarkMode) {
    if (!optionConfig) {
        optionConfig = (config_t *)malloc(sizeof(config_t));
        config_init(optionConfig);
        config_read_file(optionConfig, optionFile);
    }

    config_setting_t * folder = config_setting_get_member(config_root_setting(optionConfig), "FolderColor");
    config_setting_t * book = config_setting_get_member(config_root_setting(optionConfig), "BookColor");
    config_setting_t * scroll = config_setting_get_member(config_root_setting(optionConfig), "ScrollSpeed");
    config_setting_t * zoom = config_setting_get_member(config_root_setting(optionConfig), "ZoomAmount");
    config_setting_t * dark = config_setting_get_member(config_root_setting(optionConfig), "DarkMode");

    if (folder && book && scroll && zoom) {
        *chosenFolderColor = config_setting_get_int(folder);
        *chosenBookColor = config_setting_get_int(book);
        *scroll_speed = config_setting_get_int(scroll);
        *zoom_amount = config_setting_get_int(zoom);
        *configDarkMode = config_setting_get_bool(dark);
    }

    return 0;
}

static void save_config(unsigned int chosenFolderColor, unsigned int chosenBookColor, int scroll_speed, int zoom_amount, bool configDarkMode) {
    config_setting_t * folder = config_setting_get_member(config_root_setting(optionConfig), "FolderColor");
    config_setting_t * book = config_setting_get_member(config_root_setting(optionConfig), "BookColor");
    config_setting_t * scroll = config_setting_get_member(config_root_setting(optionConfig), "ScrollSpeed");
    config_setting_t * zoom = config_setting_get_member(config_root_setting(optionConfig), "ZoomAmount");
    config_setting_t * dark = config_setting_get_member(config_root_setting(optionConfig), "DarkMode");

    if (!folder || !book || !scroll || !zoom || !dark) {
        folder = config_setting_add(config_root_setting(optionConfig), "FolderColor", CONFIG_TYPE_INT);
        book = config_setting_add(config_root_setting(optionConfig), "BookColor", CONFIG_TYPE_INT);
        scroll = config_setting_add(config_root_setting(optionConfig), "ScrollSpeed", CONFIG_TYPE_INT);
        zoom = config_setting_add(config_root_setting(optionConfig), "ZoomAmount", CONFIG_TYPE_INT);
        dark = config_setting_add(config_root_setting(optionConfig), "DarkMode", CONFIG_TYPE_BOOL);

    }

    if (folder && book && scroll && zoom && dark) {
        config_setting_set_int(folder, chosenFolderColor);
        config_setting_set_int(book, chosenBookColor);
        config_setting_set_int(scroll, scroll_speed);
        config_setting_set_int(zoom, zoom_amount);
        config_setting_set_bool(dark, configDarkMode);
        config_write_file(optionConfig, optionFile);
    }

}

int count_valid_files(string path, list<string> allowedExtentions ) {

    int amountOfFiles = 0;
    for (const auto & entry : fs::directory_iterator(path)) {
        string filename = entry.path().filename().string();
        string extention;
        if (filename.find('.') != std::string::npos)
            extention = filename.substr(filename.find_last_of("."));
        else {
            std::error_code ec;
            if (fs::is_directory(entry, ec))
            {
                extention = "directory";
            }
            else
            {
                extention = "none";
            }
        }
        if (contains(allowedExtentions, extention)) {
            amountOfFiles++;
        }
    }

    return amountOfFiles;
}

void Menu_StartChoosing() {

    SDL_Texture *folder_textures[] = { adwaita_folder, black_folder, blue_grey_folder, blue_folder, breeze_folder, brown_folder, carmine_folder, cyan_folder, dark_cyan_folder, deep_orange_folder, green_folder, grey_folder, indigo_folder, magenta_folder, nordic_folder, orange_folder, pale_brown_folder, pale_orange_folder, pink_folder, red_folder, teal_folder, violet_folder, white_folder, yaru_folder, yellow_folder };
    SDL_Texture *book_textures[] = { adwaita_book, black_book, blue_grey_book, blue_book, breeze_book, brown_book, carmine_book, cyan_book, dark_cyan_book, deep_orange_book, green_book, grey_book, indigo_book, magenta_book, nordic_book, orange_book, pale_brown_book, pale_orange_book, pink_book, red_book, teal_book, violet_book, white_book, yaru_book, yellow_book };

    unsigned int chosenFolderColor = 0;
    unsigned int chosenBookColor = 0;
    int scroll_option = 1;
    int zoom_option = 1;

    string colors[] = { "Adwaita", "Black", "Blue_Grey", "Blue", "Breeze", "Brown", "Carmine", "Cyan", "Dark_Cyan", "Deep_Orange", "Green", "Grey", "Indigo", "Magenta", "Nordic", "Orange", "Pale_Brown", "Pale_Orange", "Pink", "Red", "Teal", "Violet", "White", "Yaru", "Yellow" };




    load_config(&chosenFolderColor, &chosenBookColor, &scroll_option, &zoom_option, &configDarkMode);

    SDL_Texture *folder_image = folder_textures[chosenFolderColor];
    SDL_Texture *book_image = book_textures[chosenBookColor];

    float zoom_amount = 0.3*zoom_option;
    int scroll_speed = 3*scroll_option;

    int chosen_index = 0;

    /* bool readingBook = false; */
    list<string> allowedExtentions = {".pdf", ".epub", ".cbz", ".xps", "directory"};
    list<string> warnedExtentions = {".epub", ".cbz", ".xps"};

    bool drawOption = false;

    int option_index = 0;
    int amountOfOptions = 4; /*Will not be a variable in the end probably a constant or will count them out in a function*/


    string path = "/switch/eBookReader/books";


    // Count the amount of allowed files
    int amountOfFiles = count_valid_files(path, allowedExtentions);

    bool isWarningOnScreen = false;
    int windowX, windowY;
    SDL_GetWindowSize(WINDOW, &windowX, &windowY);
    int warningWidth = 700;
    int warningHeight = 300;

    padConfigureInput(1, HidNpadStyleSet_NpadStandard);

    PadState pad;
    padInitializeDefault(&pad);

    while(appletMainLoop()) {


        SDL_Color textColor = configDarkMode ? WHITE : BLACK;
        SDL_Color backColor = configDarkMode ? BACK_BLACK : BACK_WHITE;

        SDL_ClearScreen(RENDERER, backColor);
		SDL_RenderClear(RENDERER);


	padUpdate(&pad);

	u64 kDown = padGetButtonsDown(&pad);
	u64 kHeld = padGetButtons(&pad);
	u64 kUp = padGetButtonsUp(&pad);

        if (kUp & HidNpadButton_Plus) {
            drawOption = !drawOption;
        }

        if (kDown & HidNpadButton_B) {
            if (isWarningOnScreen) {
                isWarningOnScreen = false;
            } else if ( path == "/switch/eBookReader/books" ) {
                if (!isWarningOnScreen && !drawOption) {
                    save_config(chosenFolderColor, chosenBookColor, scroll_option, zoom_option, configDarkMode);
                    break;
                } else {
                    isWarningOnScreen = false;
                    drawOption = false; /*later we will check for secondary option menu*/
                }
            } else if (!drawOption) {
                int final_char = path.size()-1;
                char last_char_deleted = ' ';
                while (last_char_deleted != '/') {
                    last_char_deleted = path[final_char];
                    final_char--;
                    path.pop_back();
                }
                chosen_index = 0;
                amountOfFiles = count_valid_files(path, allowedExtentions);
            } else {
                drawOption = false;
            }
        }

        if (kDown & HidNpadButton_A && !drawOption) {
            int bookIndex = 0;
            for (const auto & entry : fs::directory_iterator(path)) {
                string filename = entry.path().filename().string();
                string extention;
                if (filename.find('.') != std::string::npos)
                    extention = filename.substr(filename.find_last_of("."));
                else {
                    std::error_code ec;
                    if (fs::is_directory(entry, ec))
                    {
                        extention = "directory";
                    }
                    else
                    {
                        extention = "none";
                    }
            }

                if (contains(allowedExtentions, extention)) {
                    if (bookIndex == chosen_index) {
                        if (contains(warnedExtentions, extention)) {
                            /*#ifdef EXPERIMENTAL
                                SDL_DrawImage(RENDERER, warning, 5, 10 + (40 * highlighted_index));
                            #endif*/
                            if (isWarningOnScreen) {
                                isWarningOnScreen = false;
                                string book = path + "/" + filename;
                                cout << "Opening book: " << book << endl;

                                Menu_OpenBook((char*) book.c_str(), scroll_speed, zoom_amount);
                            } else {
                                isWarningOnScreen = true;
                            }
                        } else if (extention ==  "directory") {
                            path = path + "/" + filename;
                            amountOfFiles = count_valid_files(path, allowedExtentions);
                            chosen_index = 0;
                        } else {
                                string book = path + "/" + filename;
                                cout << "Opening book: " << book << endl;

                                Menu_OpenBook((char*) book.c_str(), scroll_speed, zoom_amount);
                                /* readingBook = true; */
                        }
                    }

                    bookIndex++;
                }
            }
        }

        if (kDown & HidNpadButton_Up || kDown & HidNpadButton_StickRUp) {
            if (chosen_index != 0 && !isWarningOnScreen && !drawOption) {
                chosen_index--;
            } else if (drawOption) {
                if (option_index != 0) {
                    option_index--;
                } else {
                    option_index = amountOfOptions-1;
                }
            } else if (chosen_index == 0) {
                chosen_index = amountOfFiles-1;
            }
        }

        if (kDown & HidNpadButton_Down || kDown & HidNpadButton_StickRDown) {
            if (chosen_index == amountOfFiles-1) {
                chosen_index = 0;
            } else if (drawOption) {
                if (option_index == amountOfOptions-1) {
                    option_index = 0;
                } else {
                    option_index++;
                }
            } else if (chosen_index < amountOfFiles-1 && !isWarningOnScreen && !drawOption) {
                chosen_index++;
            }
        }

        if (kUp & HidNpadButton_Minus) {
            configDarkMode = !configDarkMode;
        }

        if (kDown & HidNpadButton_Right && drawOption) {
            switch (option_index) {
                case 0:
                        if (chosenFolderColor < COLORS_SIZE) {
                            chosenFolderColor++;
                            folder_image = folder_textures[chosenFolderColor];
                        } else {
                            chosenFolderColor = 0;
                            folder_image = folder_textures[chosenFolderColor];
                        }
                        break;
                case 1:
                        if (chosenBookColor <= COLORS_SIZE) {
                            chosenBookColor++;
                            book_image = book_textures[chosenBookColor];
                        } else {
                            chosenBookColor = 0;
                            book_image = book_textures[chosenBookColor];
                        }
                        break;
                case 2:
                        if (scroll_option < 4 ) {
                            scroll_option++;
                            scroll_speed = SCROLL*scroll_option;
                        } else {
                            scroll_option = 1;
                            scroll_speed = SCROLL*scroll_option;
                        }
                        break;
                case 3:
                        if (zoom_option < 4) {
                            zoom_option++;
                            zoom_amount = ZOOM*zoom_option;
                        } else {
                            zoom_option = 1;
                            zoom_amount = ZOOM*zoom_option;
                        }
                        break;
                default:
                        break;
            }

        }

        if (kDown & HidNpadButton_Left && drawOption) {
            switch (option_index) {
                case 0:
                        if (chosenFolderColor > 0) {
                            chosenFolderColor--;
                            folder_image = folder_textures[chosenFolderColor];
                        } else {
                            chosenFolderColor = COLORS_SIZE;
                            folder_image = folder_textures[chosenFolderColor];
                        }
                        break;
                case 1:
                        if (chosenBookColor > 0) {
                            chosenBookColor--;
                            book_image = book_textures[chosenBookColor];
                        } else {
                            chosenBookColor = COLORS_SIZE;
                            book_image = book_textures[chosenBookColor];
                        }
                        break;
                case 2:
                        if (scroll_option > 1) {
                            scroll_option--;
                            scroll_speed= SCROLL*scroll_option;
                        } else {
                            scroll_option = 4;
                            scroll_speed = SCROLL*scroll_option;
                        }
                        break;
                case 3:
                        if (zoom_option > 1) {
                            zoom_option--;
                            zoom_amount = ZOOM*zoom_option;
                        } else {
                            zoom_option = 4;
                            zoom_amount = ZOOM*zoom_option;
                        }
                        break;
                default:
                        break;
            }

        }

        int exitWidth = 0;
        TTF_SizeText(ROBOTO_20, "Exit", &exitWidth, NULL);
        SDL_DrawButtonPrompt(RENDERER, button_b, ROBOTO_20, textColor, "Exit", windowX - exitWidth - 50, windowY - 10, 35, 35, 5, 0);

        int themeWidth = 0;
        TTF_SizeText(ROBOTO_20, "Switch Theme", &themeWidth, NULL);
        SDL_DrawButtonPrompt(RENDERER, button_minus, ROBOTO_20, textColor, "Switch Theme", windowX - themeWidth - 50, windowY - 40, 35, 35, 5, 0);

        /*For Options*/
        SDL_DrawButtonPrompt(RENDERER, button_plus, ROBOTO_20, textColor, "Options Menu", windowX - themeWidth - 50, windowY - 70, 35, 35, 5, 0);

        /*For Path*/
        SDL_DrawText(RENDERER, ROBOTO_25, 0, windowY-40, textColor, path.substr(20).c_str());

        int highlighted_index = 0;
        int option_highlight = 0;
        for (const auto & entry : fs::directory_iterator(path)) {
            string filename = entry.path().filename().string();
            string extention;
            if (filename.find('.') != std::string::npos)
                extention = filename.substr(filename.find_last_of("."));
            else {
                std::error_code ec;
                if (fs::is_directory(entry, ec))
                {
                    extention = "directory";
                }
                else
                {
                    extention = "none";
                }
            }

            if (contains(allowedExtentions, extention)) {
                if (chosen_index == highlighted_index) {
                    SDL_DrawRect(RENDERER, 15, 15 + (40 * highlighted_index), 1265, 40, configDarkMode ? SELECTOR_COLOUR_DARK : SELECTOR_COLOUR_LIGHT);
                }

                if (contains(warnedExtentions, extention)) {
                    SDL_DrawImage(RENDERER, warning, 25, 18 + (40 * highlighted_index));
                }
                if (extention == "directory") {
                    SDL_DrawImage(RENDERER, folder_image, 25, 18 + (40 * highlighted_index));
                }
                if (!contains(warnedExtentions, extention) && extention != "directory")  {
                    SDL_DrawImage(RENDERER, book_image, 25, 18 + (40 * highlighted_index));
                }




                SDL_DrawText(RENDERER, ROBOTO_25, 70, 20 + (40 * highlighted_index), textColor, entry.path().filename().c_str());

                if (isWarningOnScreen) {
                    if (!configDarkMode) { // Display a dimmed background if on light mode
                        SDL_DrawRect(RENDERER, 0, 0, 1280, 720, SDL_MakeColour(50, 50, 50, 150));
                    }

                    SDL_DrawRect(RENDERER, (windowX - warningWidth) / 2, (windowY - warningHeight) / 2, warningWidth, warningHeight, configDarkMode ? HINT_COLOUR_DARK : HINT_COLOUR_LIGHT);
                    SDL_DrawText(RENDERER, ROBOTO_30, (windowX - warningWidth) / 2 + 15, (windowY - warningHeight) / 2 + 15, textColor, "This file is not yet fully supported, and may");
                    SDL_DrawText(RENDERER, ROBOTO_30, (windowX - warningWidth) / 2 + 15, (windowY - warningHeight) / 2 + 50, textColor, "cause a system, or app crash.");
                    SDL_DrawText(RENDERER, ROBOTO_20, (windowX - warningWidth) / 2 + warningWidth - 250, (windowY - warningHeight) / 2 + warningHeight - 30, textColor, "\"A\" - Read");
                    SDL_DrawText(RENDERER, ROBOTO_20, (windowX - warningWidth) / 2 + warningWidth - 125, (windowY - warningHeight) / 2 + warningHeight - 30, textColor, "\"B\" - Cancel.");
                }

                if (drawOption){
                    int helpWidth = 680;
                    int helpHeight = 395;


                    if (!configDarkMode) { // Display a dimmed background if on light mode
                        SDL_DrawRect(RENDERER, 0, 0, 1280, 720, SDL_MakeColour(50, 50, 50, 150));
                    }

                    SDL_DrawRect(RENDERER, (windowX - helpWidth) / 2, (windowY - helpHeight) / 2, helpWidth, helpHeight, configDarkMode ? HINT_COLOUR_DARK : HINT_COLOUR_LIGHT);




                    int optTextX = (windowX - helpWidth) / 2 + 20;
                    int optTextY = (windowY - helpHeight) / 2 + 87;
                    SDL_Color textColor = configDarkMode ? WHITE : BLACK;

                    SDL_DrawRect(RENDERER, optTextX-20, optTextY + (38 * option_index), helpWidth, 40, configDarkMode ? SELECTOR_COLOUR_DARK : SELECTOR_COLOUR_LIGHT);


                    SDL_DrawText(RENDERER, ROBOTO_30, optTextX, (windowY - helpHeight) / 2 + 10, textColor, "Option Menu");

                    SDL_DrawText(RENDERER, ROBOTO_25, optTextX, optTextY, textColor, "Folder Color: ");
                    SDL_DrawText(RENDERER, ROBOTO_25, optTextX + 400, optTextY, textColor, colors[chosenFolderColor].c_str() );

                    SDL_DrawText(RENDERER, ROBOTO_25, optTextX, optTextY+38, textColor, "Book Color: ");
                    SDL_DrawText(RENDERER, ROBOTO_25, optTextX + 400, optTextY+38, textColor, colors[chosenBookColor].c_str() );

                    SDL_DrawText(RENDERER, ROBOTO_25, optTextX, optTextY+38*2, textColor, "Scroll Speed: ");
                    SDL_DrawTextf(RENDERER, ROBOTO_25, optTextX + 400, optTextY+38*2, textColor, "%d", scroll_option);

                    SDL_DrawText(RENDERER, ROBOTO_25, optTextX, optTextY+38*3, textColor, "Zoom Amount: ");
                    SDL_DrawTextf(RENDERER, ROBOTO_25, optTextX + 400, optTextY+38*3, textColor, "%d", zoom_option);
                }

                highlighted_index++;
                option_highlight++;
            }
        }

        SDL_RenderPresent(RENDERER);
    }
}
