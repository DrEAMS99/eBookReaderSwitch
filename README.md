# eBookReaderSwitch

This is a fork of eBookReaderSwitch from SeanOMik the old README will be paste on the bottom
### New Features:
* Added Folder Functionality (designs extremely inspired by the  Papirus Icon Theme)
* Added Icons that differentiate Folders from Books and Files with limited compatibility
* Saves Dark Mode settings
* Max Zoom (Up on D-Pad) in Landscape View
* Move to top of page when you go to next page in Landscape View
* Zoom speed and Scroll Speed settings

### New TODO:
* Check File Compatibility
* Maybe add ambient sounds for headphone users ex:(Rain, Fire Crackling, etc)
* Save Orientation

### New ScreenShots

New Dark Mode Options Menu:
<br></br>
<img src="screenshots/NewDarkModeOptions.jpg" width="512" height="288">
<br></br>

New Dark Mode Folders:
<br></br>
<img src="screenshots/NewDarkModeFolder.jpg" width="512" height="288">
<br></br>

New Dark Mode Books:
<br></br>
<img src="screenshots/NewDarkModeBook.jpg" width="512" height="288">
<br></br>

New Light Mode Options Menu:
<br></br>
<img src="screenshots/NewLightModeOptions.jpg" width="512" height="288">
<br></br>

New Light Mode Folders:
<br></br>
<img src="screenshots/NewLightModeFolders.jpg" width="512" height="288">
<br></br>

New Light Mode Book:
<br></br>
<img src="screenshots/NewLightModeBook.jpg" width="512" height="288">
<br></br>

# Old README:

# eBookReaderSwitch

### Features:
* Saves last page number
* Reads PDF, EPUB, CBZ, and XPS files
* Dark and light mode
* Landscape reading view
* Portrait reading view
* Touch screen controls
	* Touch the botton/top of the screen to zoom in/out and left and right to change the page.
* Books read from `/switch/eBookReader/books`

### TODO:
* Do some extra testing on file compatibility.
* 2 pages side by side in landscape.
* Hardware lock to prevent accidental touches (maybe Vol- ?) (?).
* Save orientation, and dark mode settings.

### Screen Shots:

Dark Mode Help Menu:
<br></br>
<img src="screenshots/darkModeHelp.jpg" width="322" height="178.4">
<br></br>

Dark Mode Landscape Reading (With the Switch horizonal):
<br></br>
<img src="screenshots/darkModeLandscape.jpg" width="512" height="288">
<br></br>

Dark Mode Portrait Reading (With the Switch vertical):
<br></br>
<img src="screenshots/darkModePortrait.jpg" width="285.6" height="408.8">
<br></br>

Dark Mode Book Selection:
<br></br>
<img src="screenshots/darkModeSelection.jpg" width="512" height="288">
<br></br>

Light Mode Landscape Reading:
<br></br>
<img src="screenshots/lightModeLandscape.jpg" width="512" height="288">

### Credit:
* moronigranja - For allowing more file support
* NX-Shell Team - A good amount of the code is from an old version of their application.

### Building
* Release built with [libnx release v4.1.3](https://github.com/switchbrew/libnx).
* Uses `freetype` and other libs which comes with `switch-portlibs` via `devkitPro pacman`:
```
pacman -S libnx switch-portlibs
```
then run:
```
make mupdf
make
```
to build.

If you don't have twili debugger installed, delete the `-ltwili` flag on the Makefile to compile:
```
LIBS: -ltwili
```



